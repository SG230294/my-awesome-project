import { Body, Controller, Get, Post } from '@nestjs/common';
import { FruitService } from './fruit.service';
import { CreateFruitDto } from './dto/create-fruit.dto';
import { Fruit } from './fruit.entity';

@Controller()
export class FruitController {
  constructor(private readonly fruitService: FruitService) {}

  @Get('fruit')
  async getFruit(): Promise<Fruit[]> {
    return this.fruitService.list();
  }
  @Post('fruit')
  create(@Body() createFruitDto: CreateFruitDto) {
    return this.fruitService.create(createFruitDto);
  }
}
