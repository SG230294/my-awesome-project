import { Inject, Injectable, UseInterceptors } from '@nestjs/common';
import { CreateFruitDto } from './dto/create-fruit.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Fruit } from './fruit.entity';
import { Repository } from 'typeorm';

import { Cache } from 'cache-manager';
import { CACHE_MANAGER, CacheInterceptor } from '@nestjs/cache-manager';

@Injectable()
@UseInterceptors(CacheInterceptor)
export class FruitService {
  constructor(
    @InjectRepository(Fruit)
    private readonly fruitRepository: Repository<Fruit>,
    @Inject(CACHE_MANAGER) private cacheManager: Cache,
  ) {}
  async create(createFruitDto: CreateFruitDto) {
    return await this.fruitRepository.save({
      name: createFruitDto.name,
    });
  }
  async list() {
    return await this.fruitRepository.find();
  }
}
