import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Fruit {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  name: string;
}
