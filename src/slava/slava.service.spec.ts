import { Test, TestingModule } from '@nestjs/testing';
import { SlavaService } from './slava.service';

describe('SlavaService', () => {
  let service: SlavaService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SlavaService],
    }).compile();

    service = module.get<SlavaService>(SlavaService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
