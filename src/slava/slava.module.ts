import { Module } from '@nestjs/common';
import { SlavaResolver } from './slava.resolver';
import { SlavaService } from './slava.service';

@Module({
  providers: [SlavaResolver, SlavaService],
})
export class SlavaModule {}
