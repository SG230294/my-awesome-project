import { Test, TestingModule } from '@nestjs/testing';
import { SlavaResolver } from './slava.resolver';

describe('SlavaResolver', () => {
  let resolver: SlavaResolver;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SlavaResolver],
    }).compile();

    resolver = module.get<SlavaResolver>(SlavaResolver);
  });

  it('should be defined', () => {
    expect(resolver).toBeDefined();
  });
});
