import { Args, Query, Resolver } from '@nestjs/graphql';

@Resolver()
export class SlavaResolver {
  @Query(() => String)
  async getHello(@Args('msg') msg: string) {
    return 'Hi ' + msg + '!';
  }
}
