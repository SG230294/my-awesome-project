FROM node:18-alpine AS build

WORKDIR /app
COPY . .

RUN apk add --no-cache make
RUN npm ci
COPY . .
RUN npm run build && npm prune --production


FROM node:18-alpine AS production

ENV NODE_ENV=production
WORKDIR /app

COPY --from=build /app/dist /app/dist
COPY --from=build /app/node_modules /app/node_modules
COPY ./package.json .

EXPOSE 50050
CMD ["npm", "run", "start:prod"]